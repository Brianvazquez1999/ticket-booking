package ticketbooking.moviefill.entities;

import org.springframework.stereotype.Service;
import ticketbooking.moviefill.entities.Movies;

import java.text.Format;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import ticketbooking.moviefill.repositories.MovieRepository;

@Service
public class MoviesService {


    @Autowired
    private MovieRepository movieRepository;

    public List<Movies> listMovies() {
        return movieRepository.findAll();
    }

    public void CreateMovie(Movies movie) {
        movieRepository.save(movie);
    }

    public Movies readMovie(String name) {
        return movieRepository.findByname(name);
    }

    public Optional<Movies> readMovie(Integer id) {
        return movieRepository.findById(id);
    }

    public void updateMovie(Integer id, Movies newMovie) {
        Movies movie = movieRepository.findById(id).get();
        movie.setMovieName(newMovie.getMovieName());
        movie.setImageUrl(newMovie.getImageUrl());
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        movie.setShowing(LocalDateTime.parse(newMovie.getshowing(), format));

    }

}
