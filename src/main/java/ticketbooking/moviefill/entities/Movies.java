package ticketbooking.moviefill.entities;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@Entity
@Table(name = "movies")
public class Movies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="movie_name")
    private @NotBlank String name;

    private LocalDateTime showing;

    private @NotBlank String imageUrl;

    public Movies() {

    }

    public Movies (@NotBlank String name, LocalDateTime showing) {
        this.name = name;
        this.showing = showing;

    }
    public Movies (@NotBlank String name, LocalDateTime showing, @NotBlank String imageUrl) {
        this.name = name;
        this.showing = showing;
        this.imageUrl = imageUrl;
    }

    public String getMovieName(){
        return this.name;
    }

    public void setMovieName(String name) {
        this.name = name;
    }
    public String getshowing(){
     DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
     String showingString = formatter.format(showing);
     return showingString;

    }

    @Override
    public String toString() {
        return "Movie{" + "id = " + id +
        ", name='" + name + '\'' +
        ", showing='" + getshowing() +'\'' + '}'
;    }

    public void setShowing(LocalDateTime showing) {
        this.showing = showing;
    }
    public String getImageUrl(){
        return this.imageUrl;
    }

    public void setImageUrl(String image) {
        this.imageUrl = image;
    }

    public Integer getId() {
        return this.id;
    }

}
