package ticketbooking.moviefill.controllers;

import ticketbooking.moviefill.ApiResponse;
import ticketbooking.moviefill.entities.Movies;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ticketbooking.moviefill.entities.MoviesService;
import java.util.List;
import java.util.Objects;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/movies")

public class MoviesController {


    @Autowired
    private MoviesService moviesService;

    @GetMapping("/")
    public ResponseEntity<List<Movies>> getMovies() {
        List<Movies> body = moviesService.listMovies();
        return  new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createMovie(@Valid @RequestBody Movies movie) {
        if (Objects.nonNull(moviesService.readMovie(movie.getMovieName()))) {
            return new ResponseEntity<ApiResponse>(new ApiResponse(false, "movie already exists"), HttpStatus.CONFLICT);
        }
        else {
            moviesService.CreateMovie(movie);
            return new ResponseEntity<ApiResponse>(new ApiResponse(true, "movie created"), HttpStatus.CREATED);
        }
    }

    @PostMapping("/{movieId}")
    public ResponseEntity<ApiResponse> updateMovie(@PathVariable("movieId") Integer movieId, @Valid @RequestBody Movies movie) {
        if (Objects.nonNull(moviesService.readMovie(movieId))) {
             moviesService.updateMovie(movieId, movie);
        return new ResponseEntity<ApiResponse>(new ApiResponse(true, "updated movie"), HttpStatus.OK);
        }
        return new ResponseEntity<ApiResponse>(new ApiResponse(false, "movie does not exist"), HttpStatus.NOT_FOUND);
    }
}
