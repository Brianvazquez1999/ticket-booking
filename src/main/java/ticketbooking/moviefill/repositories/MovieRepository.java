package ticketbooking.moviefill.repositories;

import ticketbooking.moviefill.entities.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movies, Integer> {

    Movies findByname(String name);

}
