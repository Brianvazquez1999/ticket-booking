package ticketbooking.moviefill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages = "ticketbooking.moviefill")
@EnableJpaRepositories(basePackages = "ticketbooking.moviefill.repositories")
@EntityScan("ticketbooking.moviefill.entities")
public class MoviefillApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviefillApplication.class, args);
	}

}
